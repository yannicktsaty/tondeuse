package com.kata.tondeuse;

import javafx.geometry.Orientation;

/*
* Position de la tondeuse
* */
public class Tondeuse  {

    //Attributs de la tondeuse
    private int x, y;

    //Orientation de la tendeuse
    private Orientation orientation;

    public Tondeuse() {
        this.orientation = Orientation.NORD;
        this.x = x;
        this.y = y;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    //getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public enum Orientation{
        NORD, EST, OUEST, SUD;
    }
    //setters
    public void setX(int val) {
        this.x = val;
    }

    public void setY(int val) {
        y = val;
    }

    public void avance(int dx, int dy) {
        x = x + dx;
        y = y + dy;
    }

    public void pivote() {
        if (orientation == Orientation.NORD) {
            orientation = Orientation.EST;
        }
        else if ( orientation == Orientation.EST) {
            orientation = Orientation.SUD;
        }
        else if (orientation == Orientation.SUD) {
            orientation = Orientation.OUEST;
        }
        else if (orientation == Orientation.OUEST) {
            orientation = Orientation.NORD;
        }
    }



}
