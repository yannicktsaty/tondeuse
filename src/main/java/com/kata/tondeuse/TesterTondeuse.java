package com.kata.tondeuse;

import java.io.*;
import java.util.Scanner;

public class TesterTondeuse {

    public static void main(String args[]) {

        Tondeuse tondeuse = new Tondeuse();

        Scanner sc = null;
        try {
            try {
                sc = new Scanner(new File("C:\\Users\\sages\\OneDrive\\Bureau\\testTondeuse.txt"));
                while (sc.hasNextLine()) {
                    for (char c : sc.next().toCharArray()){
                        System.out.println(c);
                        if(Character.compare(c,'D') == 0){
                            tondeuse.pivote();
                            System.out.println("Rotation à droite");
                        }
                        else if(Character.compare(c,'G') == 0){
                            tondeuse.pivote();
                            System.out.println("Rotation à gauche");
                        }
                        else if(Character.compare(c,'A') == 0){
                            tondeuse.avance(0,1);
                            System.out.println("tondeuse avance");

                        }
                    }
                }
            } finally {
                if (sc != null)
                    sc.close();
            }

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}