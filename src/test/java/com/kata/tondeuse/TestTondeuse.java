package com.kata.tondeuse;

import javafx.geometry.Orientation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestTondeuse {

    @Test
    public void doNothing(){
        assertFalse(true);
    }

    @Test
    public void testPositionInitAuNord(){
        Tondeuse tondeuse = new Tondeuse();
        assertEquals(Tondeuse.Orientation.NORD ,tondeuse.getOrientation());
    }

    @Test
    public void testPivoteUneFois(){
        Tondeuse tondeuse = new Tondeuse();
        tondeuse.pivote();
        assertEquals(Tondeuse.Orientation.EST,tondeuse.getOrientation());
    }

    @Test
    public void testAvance(){

    }
    //Vérifie l'arrêt hors pelouse
    @Test
    public void testArrete(){

    }


}